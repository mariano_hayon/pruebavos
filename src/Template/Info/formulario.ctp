<h1>Envianos tu consulta</h1>

<?php
	//esto crea el formulario usando los métodos de cake
	//se basa en la entidad de subida que pasamos en el controlador y luego crea los campos para llenar
    echo $this->Form->create($subida);
    echo $this->Form->control('nombre');
    echo $this->Form->control('apellido');
    echo $this->Form->control('email');
    echo $this->Form->control('dni',['label' => 'DNI (solo números)']);
    echo $this->Form->control('consulta', ['rows' => '3', 'maxlength' => "195",'label' => 'Descripción de la consulta']);
    echo $this->Form->button(__('Enviar'));
    echo $this->Form->end();
?>