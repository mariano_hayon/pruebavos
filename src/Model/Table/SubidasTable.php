<?php 
// src/Model/Table/UsersTable.php
namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\Validation\Validator;

class SubidasTable extends Table
{
	public function validationDefault(Validator $validator)
	{
		//acá van las reglas de validación, que se ejecutan luego del submit y antes de guardar la consulta
		
			//nombre no está vacío y no tiene sólo espacios en blancos
		$validator
			->allowEmpty('nombre', false)
			->notBlank('nombre','Debe escribir su nombre')
			
			//idem
			->allowEmpty('apellido', false)
			->notBlank('apellido','Debe escribir su apellido')
			
			//dni no vacío, que sean números naturales y que verifica que sea único
			->allowEmpty('dni', false)
			->naturalNumber('dni','Escriba sólo números y sin espacios')
			->add('dni', 
			['unique' => [
				'rule' => 'validateUnique', 
				'provider' => 'table', 
				'message' => 'Lo sentimos, no puede generar otra petición']
			])
			
			//email no está vacío, y verifica que seá válido
			->allowEmpty('email', false)
			->email('email',false,'Escriba un email válido')

			//consulta no vacía, con un mínimo de 10 caracteres y máximo de 200
			->allowEmpty('consulta', false)
			->minLength('consulta', 10, 'El mensaje es muy corto')
			->maxLength('consulta', 200);

		return $validator;
	}
}