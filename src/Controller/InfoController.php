<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link      https://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   https://opensource.org/licenses/mit-license.php MIT License
 */
namespace App\Controller;

use Cake\Core\Configure;
use Cake\Http\Exception\ForbiddenException;
use Cake\Http\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException;
use Cake\ORM\TableRegistry;
use Cake\Datasource\ConnectionManager;

class InfoController extends AppController
{

	public function formulario()
	{
		//carga el modelo subidas y obtiene instancia de la tabla para hacer transacciones
		$this->loadModel('Subidas');	
		$tablaSubidas = TableRegistry::get('Subidas');
		
		//setea entidad de subida
		$subida = $this->Subidas->newEntity();
		
		//vino un POST?
        if ($this->request->is('post')) 
		{
			//toma los datos de POST y los coloca en subida
			$subida = $this->Subidas->patchEntity($subida, $this->request->getData());

			//hace el intento de guardado, si hay errores los marcará en el formulario gracias a las reglas de validación
            if ($tablaSubidas->save($subida)) 
			{
				$this->Flash->success(__('Gracias por tu consulta!'));
				
				//esta redirección permite reiniciar el form
				return $this->redirect(['action' => 'formulario']); 
			}
			else
				$this->Flash->error(__('No pudimos procesar tu consulta'));
        }
		
		//con esto le pasamos la variable a la vista
        $this->set('subida', $subida);
	}
}