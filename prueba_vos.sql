-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Feb 02, 2021 at 04:15 PM
-- Server version: 5.6.17
-- PHP Version: 7.1.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `prueba_vos`
--
CREATE DATABASE IF NOT EXISTS `prueba_vos` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `prueba_vos`;

-- --------------------------------------------------------

--
-- Table structure for table `subidas`
--

CREATE TABLE IF NOT EXISTS `subidas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(50) DEFAULT NULL,
  `apellido` varchar(50) DEFAULT NULL,
  `email` varchar(85) DEFAULT NULL,
  `dni` varchar(20) DEFAULT NULL,
  `consulta` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=14 ;

--
-- Dumping data for table `subidas`
--

INSERT INTO `subidas` (`id`, `nombre`, `apellido`, `email`, `dni`, `consulta`) VALUES
(13, 'john', 'herbert doe', 'johnherbertdoe@gmail.com', '39604381', 'cuánto está el precio de la consola X-23\r\ngracias');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
